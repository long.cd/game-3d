using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Rigidbody rb;
    public int speed = 500;
    public int turningSpeed = -5;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();    
    }

    // Update is called once per frame
    void Update()
    {
        float movement = Input.GetAxis("Horizontal");
        rb.AddForce(0f, 0f, turningSpeed * movement * Time.deltaTime, ForceMode.VelocityChange);
    }
    private void FixedUpdate()
    {
        rb.AddForce(speed * Time.deltaTime, 0, 0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Obstacle"))
        {
            this.enabled = false;
            FindObjectOfType<GameManager>().EndGame();
        }
    }
}
